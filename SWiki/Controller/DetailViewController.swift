//
//  DetailViewController.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var skinColorView: UIView!
    @IBOutlet weak var vehiclesActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var originPlanetLabel: UILabel!
    @IBOutlet weak var skinColorLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabelViewHeightConstraint: NSLayoutConstraint!
    
    public var character: Character?
    var vehicleNameList: Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    private func setupUI() {
        vehiclesActivityIndicator.hidesWhenStopped = true
        activityIndicator.hidesWhenStopped = true
        
        characterNameLabel.text = character?.name
        genderLabel.text = character?.gender
        skinColorLabel.text = character?.skinColor
        
        tableView.alwaysBounceVertical = false
        tabelViewHeightConstraint.constant = CGFloat((character?.vehicles?.count)! * 44)
        
        skinColorView.layer.cornerRadius = skinColorView.frame.size.height / 2
        
        fillOriginPlanet()
        fillSkinColor()
    }
    
    private func fillOriginPlanet() {
        activityIndicator.startAnimating()
        
        RequestsManager.sharedInstance.getPlanet(planet: (character?.nativePlanet)!, success: { (planet) in
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.originPlanetLabel.text = planet
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.originPlanetLabel.text = ""
            }
        }
    }
    
    private func fillSkinColor() {
        switch character?.skinColor {
        case "fair"?:
            skinColorView.backgroundColor = .lightGray
            
            break
        case "gold"?:
            skinColorView.backgroundColor = .yellow
            
            break
        case "red"?:
            skinColorView.backgroundColor = .red
            
            break
        case "green"?:
            skinColorView.backgroundColor = .green
            
            break
        case "blue"?:
            skinColorView.backgroundColor = .blue
            
            break
        case "yellow"?:
            skinColorView.backgroundColor = .yellow
            
            break
        default:
            skinColorView.backgroundColor = .black
        }
    }
    
    @IBAction func googleMeButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "ggSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ggSegue" {
            let vc = segue.destination as! WebViewController
            vc.character = character
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension DetailViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (character?.vehicles?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "vehicleCell", for: indexPath) 
        
        cell.textLabel?.text = " "
        
        if let vehiclesList = character?.vehicles {
            if vehiclesList.count != 0 {
                vehiclesActivityIndicator.startAnimating()
                
                RequestsManager.sharedInstance.getVehicle(vehicle: vehiclesList[indexPath.row], success: { (vehicle) in
                    
                    DispatchQueue.main.async {
                        self.vehiclesActivityIndicator.stopAnimating()
                        cell.textLabel?.text = vehicle
                    }

                }, failure: { (error) in
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                    }
                })
            }
        }

        return cell
    }

}
