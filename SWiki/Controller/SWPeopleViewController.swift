//
//  FirstViewController.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import UIKit
import Gloss

class SWPeopleViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var charList: Array<Character> = []
    var loadMorePages = true
    var currentPage: Int = 1
    var selectedCharacter: Character?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        getCharacters(onPage: currentPage, clear: true)
    }
    
    private func setupUI() {
        activityIndicator.hidesWhenStopped = true
    }
    
    private func getCharacters(onPage page:Int, clear: Bool) {
        activityIndicator.startAnimating()
        
        if clear {
            charList.removeAll()
        }
        
        RequestsManager.sharedInstance.getCharactersList(onPage: page, success: { (characteres) in
            if let chars = characteres.charactersList {
                self.charList.append(contentsOf: chars)
            }
            
            self.loadMorePages = characteres.nextPageUrl != nil
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            let vc = segue.destination as! DetailViewController
            vc.character = selectedCharacter
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SWPeopleViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedCharacter = charList[indexPath.row]
        
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row == charList.count - 1 && loadMorePages) {
            currentPage += 1
            
            getCharacters(onPage: currentPage, clear: false)
        }
    }
}

extension SWPeopleViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "charCell", for: indexPath) as! CharacterTableViewCell
        
        let char = charList[indexPath.row] as Character
        
        cell.characterNameLabel.text = char.name
        cell.characterSpeciesLabel.text = ""
        
        if let species = char.species {
            if species.count != 0 {
                cell.updateSpecie(specie: species[0])
            } else {
                cell.characterSpeciesLabel.text = "Unknown Specie"
            }
        }
        
        if let vehicles = char.vehicles {
            cell.numberOfVehicles.text = String(vehicles.count)
        }

        return cell
    }
}
