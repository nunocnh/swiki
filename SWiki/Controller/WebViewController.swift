//
//  WebViewController.swift
//  SWiki
//
//  Created by Nuno Cunha on 19/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    public var character: Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let charName = character?.name {
            let encodedString = charName.replacingOccurrences(of: " ", with: "%20")
            let urlString = "http://google.com/search?q=\(encodedString)"
            
            if let url = URL(string: urlString) {
                let request = URLRequest(url: url)
                webView.load(request)
            }

        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
