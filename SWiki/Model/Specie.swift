//
//  Specie.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import Foundation
import Gloss

struct Specie: JSONDecodable {
    
    let name: String?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.name = "name" <~~ json
    }
}
