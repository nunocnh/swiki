//
//  Character.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import Foundation
import Gloss

struct Character: JSONDecodable {
    
    let name: String?
    let gender: String?
    let species: Array<String>?
    let nativePlanet: String?
    let skinColor: String?
    let vehicles: Array<String>?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.name = "name" <~~ json
        self.gender = "gender" <~~ json
        self.species = "species" <~~ json
        self.nativePlanet = "homeworld" <~~ json
        self.skinColor = "skin_color" <~~ json
        self.vehicles = "vehicles" <~~ json
    }
}
