//
//  File.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import Foundation
import Gloss

struct CharactersList: JSONDecodable {
    
    let nextPageUrl: String?
    let charactersList: Array<Character>?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.nextPageUrl = "next" <~~ json
        self.charactersList = "results" <~~ json
    }
}
