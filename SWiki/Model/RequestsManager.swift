//
//  RequestsManager.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import UIKit
import Gloss

class RequestsManager: NSObject {
    
    let baseURL = "https://swapi.co/api"
    
    static let sharedInstance = RequestsManager()
    
    private override init() {
        
    }
    
    func getCharactersList(onPage page: Int, success: @escaping (CharactersList) -> Void, failure: @escaping (Error) -> Void) {
        let session = URLSession(configuration: .default)
        let url = URL.init(string: "\(baseURL)/people/?page=\(page)")
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            do {
                let parsedData = try JSONSerialization.jsonObject(with: data!) as! JSON
                let charList = CharactersList(json: parsedData)
                
                success(charList!)
                
            } catch let error as NSError {
                failure(error)
            }
        }
        task.resume()
    }
    
    func getSpecie(specie: String, success: @escaping (String) -> Void, failure: @escaping (Error) -> Void) {
        let session = URLSession(configuration: .default)
        let url = URL.init(string: specie)
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            do {
                let parsedData = try JSONSerialization.jsonObject(with: data!) as! JSON
                let specie = Specie(json: parsedData)
                
                success((specie?.name!)!)
                
            } catch let error as NSError {
                failure(error)
            }
        }
        task.resume()
    }
    
    func getPlanet(planet: String, success: @escaping (String) -> Void, failure: @escaping (Error) -> Void) {
        let session = URLSession(configuration: .default)
        let url = URL.init(string: planet)
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            do {
                let parsedData = try JSONSerialization.jsonObject(with: data!) as! JSON
                let planet = Planet(json: parsedData)
                
                success((planet?.name!)!)
                
            } catch let error as NSError {
                failure(error)
            }
        }
        task.resume()
    }
    
    func getVehicle(vehicle: String, success: @escaping (String) -> Void, failure: @escaping (Error) -> Void) {
        let session = URLSession(configuration: .default)
        let url = URL.init(string: vehicle)
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            do {
                let parsedData = try JSONSerialization.jsonObject(with: data!) as! JSON
                let vehicle = Ship(json: parsedData)
                
                success((vehicle?.name!)!)
                
            } catch let error as NSError {
                failure(error)
            }
        }
        task.resume()
    }
    
}
