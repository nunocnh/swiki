//
//  CharacterTableViewCell.swift
//  SWiki
//
//  Created by Nuno Cunha on 18/01/18.
//  Copyright © 2018 Nuno Cunha. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var characterSpeciesLabel: UILabel!
    @IBOutlet weak var numberOfVehicles: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.activityIndicator.hidesWhenStopped = true
    }
    
    func updateSpecie(specie: String) {
        self.activityIndicator.startAnimating()
        RequestsManager.sharedInstance.getSpecie(specie: specie, success: { (specie) in

            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.characterSpeciesLabel.text = specie
            }

        }) { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
